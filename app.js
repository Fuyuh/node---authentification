// IMPORT
const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const cookieParser = require('cookie-parser')

let config = require('./knexfile.js')
let database = knex(config.development)


// IMPORT CUSTOM MDW
const createAccMDW = require('./middlewares/create-account')
const confirmAccMDW = require('./middlewares/confirm-registration')
const activateAccMDW = require('./middlewares/activate-account')
const connexionMDW = require('./middlewares/connexion')
const connexionAjaxMDW = require('./middlewares/connexion_ajax')
const autorisationMDW = require('./middlewares/access_Admin')

const ownDataMDW = require('./middlewares/access_noAdmin')


// INITIALIZE
const app = express(feathers())
app.use(cookieParser())

// PARSE
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// MIDDLEWARES
app.post('/create-account', createAccMDW)
app.get('/confirm-registration', confirmAccMDW)
app.post('/activate-account', activateAccMDW) 

app.get('/api/:table/:id', ownDataMDW(database))
app.use('/api/:table', autorisationMDW(database))

// CONNEXION
app.use(express.json())
app.post('/loginajax', connexionAjaxMDW(database))
app.post('/login', connexionMDW(database))

// STATIC FILES
app.use('/', express.static('./static/index'))
app.use('/register', express.static('./static/register'))
app.use('/login', express.static('./static/login'))
app.use('/loginajax', express.static('./static/loginajax'))
app.use('/home', express.static('./static/home'))

// REST
app.configure(express.rest())
app.use('/api/users', service({
  Model: database,
  name: 'User'
}))
app.use('/api/pictures', service({
  Model: database,
  name: 'Picture'
}))

// ERROR
app.use(express.errorHandler())

// START SERVER
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
