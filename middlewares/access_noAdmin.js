const jwt = require('jsonwebtoken')
const JWT_SIGN_SECRET = require('../config/jwt_secret')

module.exports = (database) =>
     async (req, res, next) => {
        if(req.cookies.token) {
    
            jwt.verify(req.cookies.token, JWT_SIGN_SECRET, async (err, payload) => {
    
                if (err) return res.send("Wrong token")
                
                // CHECK WHICH TABLE
                let table = ""
                let queryDict = {}
                if (req.params.table === "users") {
                    table = 'User'
                    queryDict['id'] = req.params.id
                }
                else if (req.params.table === "pictures") {
                    table = 'Picture'
                    queryDict['user_id'] = req.params.id
                }
                // GET USER DATA
                let key = Object.keys(queryDict)[0] // dynamic column

                if (payload.id == queryDict[key]) {
                    let user = await database(table).select().where(queryDict)
                    res.send(user)
                } else {
                    res.sendStatus(403)
                }
                

            })
        } else { // NOT CONNECTED
            res.redirect('/')
        }
    }
 