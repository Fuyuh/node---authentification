const bcrypt = require('bcrypt')

const jwt = require('jsonwebtoken')
let JWT_SIGN_SECRET = require('../config/jwt_secret')

module.exports = (database) =>
async (request, response) => {
    
    let userDB = await database('User').select().where({email: request.body.email})

    if (userDB.length) { // Si l'utilisateur existe

        let token = jwt.sign({id: userDB[0].id}, JWT_SIGN_SECRET, { algorithm: 'HS256', expiresIn: '10m' })
        if (userDB[0].active) {

            let userDB_password = userDB[0].password // DB PSW HASH

            await bcrypt.compare(request.body.password, userDB_password, (err, pswmatch) => {
                if (err) throw err
                if (!pswmatch) return response.send("Mauvais mot de passe")
                if (pswmatch) {
                    response.cookie('token', token, { maxAge: 600000 })
                    response.redirect("/home")
                }
            })

        } else {
        response.send("Votre compte n'est pas activé")
        }

    } else {
        response.send("L'utilisateur n'existe pas")
    }
}