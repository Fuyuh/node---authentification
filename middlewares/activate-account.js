const bcrypt = require('bcrypt')

const knex = require('knex')
let config = require('../knexfile.js')
let database = knex(config.development)

module.exports = async (req, res) => {

    let saltRounds = 5
    let hash = bcrypt.hashSync(req.body.password, saltRounds)
  
    await database('User').update({active: 1, password : hash}).where({email: req.body.email})
  
    res.redirect('/login')
}