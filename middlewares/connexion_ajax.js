const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
let JWT_SIGN_SECRET = require('../config/jwt_secret')

module.exports = (database) =>
    async (request, response, next) => {
    
        const user = await database.select('*').from('User').where('email', '=', request.body.email)

        const compare = await bcrypt.compareSync(request.body.password, user[0].password)

        if (compare) { 
            const token = jwt.sign({ id: user[0].id }, JWT_SIGN_SECRET, { algorithm: 'HS256', expiresIn: '10m' })
            response.cookie('token', token, { maxAge: 600000 })
            response.sendStatus(200)
        } else {
            response.sendStatus(403)
        }
        next()
    }