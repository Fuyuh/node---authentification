const jwt = require('jsonwebtoken')
const JWT_SIGN_SECRET = require('../config/jwt_secret')

module.exports = (database) =>
    async (req, res, next) => {
    
        if(req.cookies.token) {
    
            jwt.verify(req.cookies.token, JWT_SIGN_SECRET, async (err, payload) => {
    
                if (err) return res.send("Wrong token")
                
                // GET USER INFO
                let queryDict = {}
                queryDict['id'] = payload.id

                let [userConnected] = await database('User').select().where(queryDict)

                    // SHOW DATA
                    if (userConnected.is_admin) { next() }
                    else { res.sendStatus(403) }

            })

        } else { // NOT CONNECTED
            res.redirect('/')
    
      }
    }
